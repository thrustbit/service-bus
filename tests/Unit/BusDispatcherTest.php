<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus\Unit;

use Prooph\Common\Event\ActionEvent;
use Prooph\ServiceBus\CommandBus;
use Prooph\ServiceBus\EventBus;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\QueryBus;
use Thrustbit\ServiceBus\BusDispatcher;
use Thrustbit\ServiceBus\CommandBusManager;
use Thrustbit\ServiceBus\EventBusManager;
use Thrustbit\ServiceBus\QueryBusManager;
use ThrustbitTests\ServiceBus\Mock\SomeCommand;
use ThrustbitTests\ServiceBus\Mock\SomeEvent;
use ThrustbitTests\ServiceBus\Mock\SomeQuery;
use ThrustbitTests\ServiceBus\TestCase;

class BusDispatcherTest extends TestCase
{
    /**
     * @test
     */
    public function it_dispatch_a_command_message(): void
    {
        $bus = new CommandBus();
        $message = new SomeCommand(['todo' => 'better tests']);

        $receivedMessage = null;
        $dispatchEvent = null;
        $bus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent) use (&$receivedMessage, &$dispatchEvent): void {
                $actionEvent->setParam(
                    MessageBus::EVENT_PARAM_MESSAGE_HANDLER,
                    function (SomeCommand $someCommand) use (&$receivedMessage): void {
                        $receivedMessage = $someCommand;
                    }
                );
                $dispatchEvent = $actionEvent;
            },
            MessageBus::PRIORITY_ROUTE
        );

        $this->mc->expects($this->once())->method('resolveBus')->willReturn($bus);
        $this->getDispatcher()->dispatch($message, 'foo_bus');

        $this->assertSame($message, $receivedMessage);
        $this->assertTrue($dispatchEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED));
    }

    /**
     * @test
     */
    public function it_dispatch_a_query_message(): void
    {
        $bus = new QueryBus();
        $message = new SomeQuery(['todo' => 'better tests']);

        $receivedMessage = null;
        $dispatchEvent = null;
        $bus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent) use (&$receivedMessage, &$dispatchEvent): void {
                $actionEvent->setParam(
                    MessageBus::EVENT_PARAM_MESSAGE_HANDLER,
                    function (SomeQuery $someQuery) use (&$receivedMessage): void {
                        $receivedMessage = $someQuery;
                    }
                );
                $dispatchEvent = $actionEvent;
            },
            MessageBus::PRIORITY_ROUTE
        );

        $this->mq->expects($this->once())->method('resolveBus')->willReturn($bus);
        $this->getDispatcher()->dispatch($message, 'foo_bus');

        $this->assertSame($message, $receivedMessage);
        $this->assertTrue($dispatchEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED));
    }

    /**
     * @test
     */
    public function it_dispatch_an_event_message(): void
    {
        $bus = new EventBus();
        $message = new SomeEvent(['todo' => 'better tests']);

        $receivedMessage = null;
        $dispatchEvent = null;
        $bus->attach(
            MessageBus::EVENT_DISPATCH,
            function (ActionEvent $actionEvent) use (&$receivedMessage, &$dispatchEvent): void {
                $actionEvent->setParam(
                    EventBus::EVENT_PARAM_EVENT_LISTENERS,
                    [
                        function (SomeEvent $someEvent) use (&$receivedMessage): void {
                            $receivedMessage = $someEvent;
                        }
                    ]
                );
                $dispatchEvent = $actionEvent;
            },
            MessageBus::PRIORITY_ROUTE
        );

        $this->me->expects($this->once())->method('resolveBus')->willReturn($bus);
        $this->getDispatcher()->dispatch($message, 'foo_bus');

        $this->assertSame($message, $receivedMessage);
        $this->assertTrue($dispatchEvent->getParam(MessageBus::EVENT_PARAM_MESSAGE_HANDLED));
    }

    private function getDispatcher()
    {
        return new BusDispatcher($this->mc, $this->mq, $this->me);
    }

    private $mc;
    private $mq;
    private $me;

    protected function setUp()
    {
        parent::setUp();

        $this->mc = $this->getMockBuilder(CommandBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['resolveBus'])
            ->getMock();

        $this->mq = $this->getMockBuilder(QueryBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['resolveBus'])
            ->getMock();

        $this->me = $this->getMockBuilder(EventBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods(['resolveBus'])
            ->getMock();
    }
}