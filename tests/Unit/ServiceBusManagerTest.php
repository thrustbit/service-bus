<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus\Unit;

use Prooph\ServiceBus\MessageBus;
use ThrustbitTests\ServiceBus\Mock\MessageBusManager;
use ThrustbitTests\ServiceBus\TestCase;

class ServiceBusManagerTest extends TestCase
{
    private $bus;

    protected function setUp()
    {
        parent::setUp();

        $this->bus = $this->getMockForAbstractClass(MessageBus::class);
    }

    /**
     * @test
     */
    public function it_return_a_default_service_bus_instance_when_no_bus_id_provided(): void
    {
        $m = $this->getMockManager();
        $m->expects($this->once())->method('getDefaultServiceBus')->willReturn('foo');
        $m->expects($this->never())->method('getRoutesForService')->willReturn([]);
        $m->expects($this->once())->method('resolveBus')->willReturn($this->bus);

        $this->assertInstanceOf(MessageBus::class, $m->create());
    }

    /**
     * @test
     */
    public function it_return_same_bus_instance(): void
    {
        $m = $this->getMockManager();
        $m->expects($this->never())->method('getDefaultServiceBus')->willReturn('foo');
        $m->expects($this->never())->method('getRoutesForService')->willReturn([]);
        $m->expects($this->once())->method('resolveBus')->willReturn($this->bus);

        $this->assertInstanceOf(MessageBus::class, $m->create('foo'));
        $this->assertSame($this->bus, $m->create('foo'));
    }

    private function getMockManager()
    {
        return $this->getMockBuilder(MessageBusManager::class)
            ->disableOriginalConstructor()
            ->setMethods([
                'getDefaultServiceBus', 'resolveBus', 'createBus', 'attachPlugins', 'generateRouter',
                'registerServiceLocator', 'getServiceBusConfig', 'getRoutesForService', 'mergeOptions'
            ])
            ->getMock();
    }
}