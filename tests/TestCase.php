<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus;

use Illuminate\Contracts\Container\Container;
use Illuminate\Foundation\Application;

class TestCase extends \PHPUnit_Framework_TestCase
{
    protected $app;

    protected function setUp()
    {
        $this->app = new Application();
        $this->app->instance(Container::class, \Illuminate\Container\Container::getInstance());
    }
}