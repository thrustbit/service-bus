<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus\Mock;

use Thrustbit\ServiceBus\ServiceBusManager;

class MessageBusManager extends ServiceBusManager
{

    public function busType(): string
    {
        return 'foo';
    }
}