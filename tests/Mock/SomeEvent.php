<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus\Mock;

use Prooph\Common\Messaging\DomainEvent;
use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;

class SomeEvent extends DomainEvent implements PayloadConstructable
{
    use PayloadTrait;
}