<?php

declare(strict_types=1);

namespace ThrustbitTests\ServiceBus\Mock;

use Prooph\Common\Messaging\PayloadConstructable;
use Prooph\Common\Messaging\PayloadTrait;
use Prooph\Common\Messaging\Query;

class SomeQuery extends Query implements PayloadConstructable
{
    use PayloadTrait;
}