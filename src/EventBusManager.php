<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class EventBusManager extends ServiceBusManager
{
    public function event(string $busName = null): MessageBus
    {
        return parent::create($busName);
    }

    public function busType(): string
    {
        return 'event';
    }
}