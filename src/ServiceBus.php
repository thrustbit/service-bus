<?php

namespace Thrustbit\ServiceBus;

use Prooph\Common\Messaging\Message;

interface ServiceBus
{
    /**
     * @param Message $message
     * @param string|null $busName
     *
     * @return mixed
     */
    public function dispatch(Message $message, string $busName = null);
}