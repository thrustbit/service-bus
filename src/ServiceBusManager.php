<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Prooph\ServiceBus\MessageBus;
use Prooph\ServiceBus\Plugin\Plugin;
use Prooph\ServiceBus\Plugin\Router\AsyncSwitchMessageRouter;
use Prooph\ServiceBus\Plugin\ServiceLocatorPlugin;

abstract class ServiceBusManager
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Repository
     */
    private $config;

    /**
     * @var string
     */
    private $namespace = 'laraprooph.laraprooph';

    /**
     * @var array
     */
    private $bus = [];

    public function __construct(Container $container, Repository $config)
    {
        $this->container = $container;
        $this->config = $config;
    }

    public function create(string $busName = null): MessageBus
    {
        if (isset($this->bus[$busName])) {
            return $this->bus[$busName];
        }

        $busName = $busName ?? $this->getDefaultServiceBus();

        return $this->bus[$busName] = $this->resolveBus($busName);
    }

    public function getDefaultServiceBus(): string
    {
        $defaultBus = $this->config->get($this->namespace . '.defaults.' . $this->busType());

        if (!$defaultBus) {
            throw new \InvalidArgumentException(
                sprintf('Unable to locate a default bus for %s bus type.', $this->busType())
            );
        }

        return $defaultBus;
    }

    protected function resolveBus(string $busName): MessageBus
    {
        $config = $this->getServiceBusConfig($busName);

        if (empty($config)) {
            throw new \InvalidArgumentException(
                sprintf('Unable to locate configuration for bus name %s', $busName)
            );
        }

        return $this->createBus($config, $this->getRoutesForService($busName));
    }

    protected function createBus(array $config, array $routes): MessageBus
    {
        $bus = new $config['concrete'];
        $options = $this->mergeOptions($config['options'] ?? []);

        $options['plugins'][] = $this->registerServiceLocator();
        $this->attachPlugins($bus, $options['plugins']);

        $this->generateRouter($config['router'], $routes)->attachToMessageBus($bus);

        return $bus;
    }

    protected function attachPlugins(MessageBus $bus, array $plugins): void
    {
        foreach ($plugins as $plugin) {
            $this->container->bindIf($plugin);
            $this->container->make($plugin)->attachToMessageBus($bus);
        }
    }

    protected function generateRouter(array $router, array $routes): Plugin
    {
        $routerInstance = new $router['concrete']($routes);

        if (isset($router['async_switch'])) {
            $producer = $this->container->make($router['async_switch']);
            $routerInstance = new AsyncSwitchMessageRouter($routerInstance, $producer);
        }

        return $routerInstance;
    }

    protected function registerServiceLocator(): string
    {
        $this->container->bindIf(
            ServiceLocatorPlugin::class, function (Application $app) {
            return new ServiceLocatorPlugin($app);
        });

        return ServiceLocatorPlugin::class;
    }

    protected function getServiceBusConfig(string $busName): array
    {
        return $this->config->get($this->namespace . '.dispatcher.' . $this->busType() . '.' . $busName, []);
    }

    protected function getRoutesForService(string $busName): array
    {
        return $this->config->get($this->namespace . '.router.' . $busName . '.router.routes', []);
    }

    protected function mergeOptions(array $options): array
    {
        $globalOptions = $this->config->get($this->namespace . '.options', []);

        return array_merge($globalOptions, $options ?? []);
    }

    abstract public function busType(): string;
}