<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class CommandBusManager extends ServiceBusManager
{
    public function command(string $busName = null): MessageBus
    {
        return parent::create($busName);
    }

    public function busType(): string
    {
        return 'command';
    }
}