<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Prooph\Common\Messaging\Message;

class BusDispatcher implements ServiceBus
{
    /**
     * @var CommandBusManager
     */
    private $commandBusManager;

    /**
     * @var QueryBusManager
     */
    private $queryBusManager;

    /**
     * @var EventBusManager
     */
    private $eventBusManager;

    public function __construct(CommandBusManager $commandBusManager,
                                QueryBusManager $queryBusManager,
                                EventBusManager $eventBusManager)
    {
        $this->commandBusManager = $commandBusManager;
        $this->queryBusManager = $queryBusManager;
        $this->eventBusManager = $eventBusManager;
    }

    public function dispatch(Message $message, string $busName = null)
    {
        switch ($message) {
            case $message->messageType() === 'command':
                $this->commandBusManager->command($busName)->dispatch($message);
                break;

            case $message->messageType() === 'event':
                $this->eventBusManager->event($busName)->dispatch($message);
                break;

            case $message->messageType() === 'query':
                return $this->queryBusManager->query($busName)->dispatch($message);
        }
    }
}