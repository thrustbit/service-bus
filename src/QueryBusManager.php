<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Prooph\ServiceBus\MessageBus;

class QueryBusManager extends ServiceBusManager
{

    public function query(string $busName = null): MessageBus
    {
        return parent::create($busName);
    }

    public function busType(): string
    {
        return 'query';
    }
}