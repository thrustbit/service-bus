<?php

declare(strict_types=1);

namespace Thrustbit\ServiceBus;

use Illuminate\Support\ServiceProvider;

class DispatcherServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @var array
     */
    protected $managers = [
        CommandBusManager::class, EventBusManager::class, QueryBusManager::class
    ];

    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/service_bus.php' =>
                config_path('service_bus.php')
        ], 'laraprooph');
    }

    public function register(): void
    {
        $this->registerConfiguration();

        $this->registerManagers();

        $this->registerServiceBus();
    }

    protected function registerConfiguration(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/service_bus.php', 'laraprooph');
    }

    protected function registerManagers(): void
    {
        foreach ($this->managers as $manager) {
            $this->app->singleton($manager);
        }
    }

    public function provides(): array
    {
        return $this->managers;
    }

    protected function registerServiceBus()
    {
        $this->app->bind(ServiceBus::class, BusDispatcher::class);
    }
}