<?php

return [

    'laraprooph' => [

        'defaults' => [
            'command' => 'command_bus',
            'query' => 'query_bus',
            'event' => 'event_bus',
        ],

        /**
         * Global options merge with bus options
         */
        'options' => [

            'message_factory' => \Prooph\Common\Messaging\MessageFactory::class,
            'plugins' => []
        ],

        'dispatcher' => [

            'command' => [

                'command_bus' => [

                    'concrete' => \Prooph\ServiceBus\CommandBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\CommandRouter::class
                    ],
                    'options' => [
                        'plugins' => []
                    ]
                ],
            ],

            'event' => [

                'event_bus' => [

                    'concrete' => \Prooph\ServiceBus\EventBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\EventRouter::class
                    ],
                    'options' => [
                        'plugins' => [
                            \Prooph\ServiceBus\Plugin\InvokeStrategy\OnEventStrategy::class
                        ]
                    ]
                ]
            ],

            'query' => [

                'query_bus' => [

                    'concrete' => \Prooph\ServiceBus\QueryBus::class,
                    'router' => [
                        'concrete' => \Prooph\ServiceBus\Plugin\Router\QueryRouter::class
                    ],
                    'options' => [
                        'plugins' => []
                    ]
                ],
            ]
        ],

        'router' => [

            /**
             * Routes for command bus service id
             */
            'command_bus' => [

                'router' => [
                    'routes' => []
                ]
            ],

            /**
             * Routes for query bus service id
             */
            'query_bus' => [

                'router' => [
                    'routes' => []
                ]
            ],

            /**
             * Routes for event bus service id
             */
            'event_bus' => [

                'router' => [
                    'routes' => []
                ]
            ],

        ]
    ]
];